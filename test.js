let assert = require('assert');
let Navigation = require('./services/navigation');
let navigateBySequence = require('./services/navigateBySequence');

describe('Navigation Service tests:', function() {
  describe('#moveForward', function() {
    it('Should return (0, 2, EAST) for Original coordinates: (0, 1, NORTH)', function() {
        let navigationService = new Navigation(0, 1, 'NORTH');
        navigationService.moveForward();
        assert.equal(navigationService.currentCoordinate(), '(0, 2, NORTH)');
    });
  });

  describe('#moveBackward', function() {
    it('Should return (2, 2, EAST) for Original coordinates: (3, 2, EAST)', function() {
        let navigationService = new Navigation(3, 2, 'EAST');
        navigationService.moveBackwards();
        assert.equal(navigationService.currentCoordinate(), '(2, 2, EAST)');
    });
  });

  describe('#rotateLeft', function() {
    it('Should return (-1, -5, WEST) for Original coordinates: (-1, -5, NORTH)', function() {
        let navigationService = new Navigation(-1, -5, 'NORTH');
        navigationService.rotate('left');
        assert.equal(navigationService.currentCoordinate(), '(-1, -5, WEST)');
    });
  });

  describe('#rotateRight', function() {
    it('Should return (-10, -2, NORTH) for Original coordinates: (-10, -2, WEST)', function() {
        let navigationService = new Navigation(-10, -2, 'WEST');
        navigationService.rotate('right');
        assert.equal(navigationService.currentCoordinate(), '(-10, -2, NORTH)');
    });
  });

  describe('#moveBasedOnSequences', function() {
    it('Should return (6, 4, NORTH) for Original coordinates: (4, 2, EAST)', function() {
        let navigationService = new Navigation(4, 2, 'EAST');
        let navigationSequence = ['F', 'L', 'F', 'F', 'F', 'R', 'F', 'L', 'B'];
        navigateBySequence(navigationService, navigationSequence);
        assert.equal(navigationService.currentCoordinate(), '(6, 4, NORTH)');
    });
  });
});