/**
 * Navigation service
 * Directions:
 * - NORTH, SOUTH -> Y - axis
 * - EAST, WEST   -> x - axis
 */

const N = 'NORTH';
const S = 'SOUTH';
const E = 'EAST';
const W = 'WEST';

class Navigation {

    constructor(x, y, direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    /**
     * Updates the X/Y coordinates based on the current direction
     */
    moveForward() {
        switch(this.direction) {
            case N:
                this.y = this.y + 1;
                break;
            case S:
                this.y = this.y - 1;
                break;
            case E:
                this.x = this.x + 1;
                break;
            case W:
                this.x = this.x - 1;
                break;
            default:
                // do nothing
        }
    }

    /**
     * Updates the X/Y coordinates based on the current direction
     */
    moveBackwards() {
        switch(this.direction) {
            case N:
                this.y = this.y - 1;
                break;
            case S:
                this.y = this.y + 1;
                break;
            case E:
                this.x = this.x - 1;
                break;
            case W:
                this.x = this.x + 1;
                break;
            default:
                // do nothing
        }
    }

    /**
     * 
     * @param {STRING (left|right)} side 
     * Updates the current direction (instance variable) based on the expected direction flow
     * Left: North -> West -> South -> East
     * Right: North -> East -> South -> West
     * The modular operation ensures that direction array index stays within the limits
     */
    rotate(side) {
        let leftDirections = [N, W, S, E];
        let rightDirections = [N, E, S, W];
        let applicableDirections = side == 'left' ? leftDirections.slice() : rightDirections.slice();
        let currentIndex = applicableDirections.indexOf(this.direction);
        this.direction = applicableDirections[(currentIndex + 1) % 4];
    }

    currentCoordinate() {
        return `(${this.x}, ${this.y}, ${this.direction})`;
    }
}

module.exports = Navigation;