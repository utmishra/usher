function navigateBySequence(navigationObj, sequenceSteps) {
    for(let i = 0; i < sequenceSteps.length; i++) {
        let currentStep = sequenceSteps[i].toUpperCase();
        // console.log('Current step', currentStep);
        switch(currentStep) {
            case 'F':
                navigationObj.moveForward();
                // console.log('Moving forward');
                break;
            case 'B':
                navigationObj.moveBackwards();
                // console.log('Moving backward');
                break;
            case 'L':
                navigationObj.rotate('left');
                // console.log('Turning left');
                break;
            case 'R':
                navigationObj.rotate('right');
                // console.log('Turning Right');
                break;
            default:
                // do nothing
        }
    }
    return navigationObj;
}

module.exports = navigateBySequence;