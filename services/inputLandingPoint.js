let readlineSync = require('readline-sync');

function takeValidInput(type, text) {
    let input = null;
    let validInput = false;
    while(!validInput) {
        let validityTest = false;
        input = readlineSync.question(`Please enter the ${text}: `);        
        if(type == 'coordinate') {
            try {
                input = parseInt(input)
            }
            catch(error) {
                continue;
            }
            validityTest = Number.isInteger(input);
        }
        else {
            validityTest = ['north', 'west', 'east', 'south'].includes(input.toLowerCase());
        }
        if(validityTest) {
            validInput = true;
        }
        else {
            console.error("Uh oh! You sure that's correct?");
        }
    }
    return input;
}

function inputLandingPoint() {
    // Fetching co-ordinates from console
    console.info('Houston! We do not have a problem!');
    console.info('Just tell us where you landed! ;-)');

    let inputsMeta = [
        ['coordinate', 'X-axis'],
        ['coordinate', 'Y-axis'],
        ['direction', 'direction'],
    ]

    let inputs = [];
    for(let i = 0; i < inputsMeta.length; i++) {
        inputs.push(takeValidInput(inputsMeta[i][0], inputsMeta[i][1]));
    }
    return inputs;
}

module.exports = inputLandingPoint;