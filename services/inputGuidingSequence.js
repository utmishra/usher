let readlineSync = require('readline-sync');

function inputGuidingSequence() {
    let input;
    let validInput = false;    
    console.info(`
        F -> Move forward on current heading
        B -> Move backwars on current heading
        L -> Rotate left by 90 degrees
        R -> Rotate right by 90 degrees
    `)
    while(!validInput) {
        input = readlineSync.question(`Please input the direction sequence (using only the initials mentioned above): `);
        if(input.match(/^[FfBbLlRr]*$/) !== null) {
            validInput = true;
        }
    }
    return input;
}

module.exports = inputGuidingSequence;