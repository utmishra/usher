let inputLandingPoint = require('./services/inputLandingPoint');
let inputGuidingSequence = require('./services/inputGuidingSequence');
let navigateBySequence = require('./services/navigateBySequence');
let Navigation = require('./services/navigation');

// Clear the screen
console.clear();

console.info(`
                 \`..-:-.-...                      
                .. :\` \`:/:..:/:\`                  
                :  -..:o//   \`:::\`                
                 .-:::../::..- :\`-                
                   ...\`.- :..::--                 
                 \`-\`  \`:  -/......-:              
                -.     ....      .-:\`\`            
               /.............\`  -..\` -.           
               :             \`.--\`-\`-.\`           
               :               .\`.\` ::....-       
        ...-::+-               -   o-....-:-      
        /::--:o.               :  -::-::-/ -      
        /::---+\`               : \`+:::-::/-.:     
        /:::--+                : /---.../+/+/     
        /::::-/:/-.-....\`\`     :/:------/o:-:-    
        :.--:-./++.+-.  \`\`.....-:.......:/+o-.    
        :--..-:/+-/-            --.....-/ :-\`     
         --::::/:-               --:---.-.   
`)

// Fetch data from the user
let inputs = inputLandingPoint();
let navigationService = new Navigation(inputs[0], inputs[1], inputs[2].toUpperCase());

console.info('Great! You entered', navigationService.currentCoordinate());
console.info("Let's guide the Rover now!");

// Takes the guiding sequence input
let sequence = inputGuidingSequence();
let sequenceSteps = sequence.split('');

// Navigates based on the guiding sequence
navigateBySequence(navigationService, sequenceSteps);

console.info('Awesome, now the Rover is at: ', navigationService.currentCoordinate());