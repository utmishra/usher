# Usher: Mars guiding rover                                          
                                                  
                 `..-:-.-...                      
                .. :` `:/:..:/:`                  
                :  -..:o//   `:::`                
                 .-:::../::..- :`-                
                   ...`.- :..::--                 
                 `-`  `:  -/......-:              
                -.     ....      .-:``            
               /.............`  -..` -.           
               :             `.--`-`-.`           
               :               .`.` ::....-       
        ...-::+-               -   o-....-:-      
        /::--:o.               :  -::-::-/ -      
        /::---+`               : `+:::-::/-.:     
        /:::--+                : /---.../+/+/     
        /::::-/:/-.-....``     :/:------/o:-:-    
        :.--:-./++.+-.  ``.....-:.......:/+o-.    
        :--..-:/+-/-            --.....-/ :-`     
         --::::/:-               --:---.-.      

## Repository URL
- [Bitbucket](https://bitbucket.org/utmishra/usher/src/main/)
- Privately shared with Rafal, Kevin & Bilal

## What it does
- The app interacts with the Mars rover and navigates the Rover so it can move around based on the input directions

## How to guide the rover
- Enter the current coordinates and direction of the Rover
- Enter the instruction string which can guide the Rover
- The app will share the final position of the Rover with the direction

## Stack
- NodeJS (Current: v14.17.4, LTS)

## How to run
- Install NodeJS (Download available [here](https://nodejs.org/en/))
```
# In the project's root directory
npm install
npm start
```

### Running test cases
```
npm test
```